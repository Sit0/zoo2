package pl.codementors.zoo.animals;

/**
 * Just an animal.
 *
 * @author psysiu
 */
public class Animal {

    /**
     * Animal name.
     */
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
